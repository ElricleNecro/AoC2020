#ifndef UTILS_C_QRSWJMWI
#define UTILS_C_QRSWJMWI

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/**
 * Count the number of line in a file.
 *
 * @param stream file to count from.
 * @return the number of line present.
 */
size_t line_count(FILE *stream) {
	size_t lines = 0;
	char c = EOF, prev = EOF;

	for(c=fgetc(stream), prev=EOF; c != EOF; prev = c, c = fgetc(stream) )
		if( c == '\n' )
			lines++;

	if( prev != '\n' )
		lines++;

	rewind(stream);

	return lines;
}

/** Array struct. */
struct array {
	/** data */
	void *data;
	/** array size */
	size_t size;
};

/**
 * Create an array of given size.
 *
 * @param size Array size.
 * @return a struct array.
 */
struct array array_create(size_t size) {
	return (struct array){ .data = malloc(size), .size = size};
}

/**
 * Free the array.
 *
 * @param arr array to free.
 */
void array_free(struct array *arr) {
	free(arr->data);
	arr->size = 0;
}

typedef struct _chained_lst_t {
	void *data;

	struct _chained_lst_t *next, *prev;
} *ChainedLst_elem;

typedef struct _chaine_lst_root_t {
	size_t len;

	struct _chained_lst_t *start, *end;
} *ChainedLst;

ChainedLst lst_new(void) {
	ChainedLst root = malloc(sizeof(struct _chaine_lst_root_t));

	root->len = 0;
	root->start = NULL;
	root->end = NULL;

	return root;
}

#define LST_FOREACH(type, var, from, code) { \
	for(ChainedLst_elem _var = from->start; _var != NULL; _var = _var->next) { \
		type var = (type)_var->data; \
		code; \
	} \
}

ChainedLst lst_append(ChainedLst root, void *data) {
	if( root == NULL ) {
		root = lst_new();
	}

	ChainedLst_elem new = malloc(sizeof(struct _chained_lst_t));

	new->data = data;

	if( root->end != NULL )
		root->end->next = new;

	if( root->start == NULL )
		root->start = new;

	new->prev = root->end;
	new->next = NULL;

	root->end = new;
	root->len++;

	return root;
}

bool lst_in(ChainedLst root, void *data, bool (*cmp)(void*, void*)) {
	if( root == NULL )
		return false;

	for(ChainedLst_elem elem = root->start; elem != NULL; elem = elem->next) {
		if( cmp(data, elem->data) )
			return true;
	}

	return false;
}

void lst_free(ChainedLst root, void (*custom_free)(void*)) {
	if( root == NULL )
		return ;

	ChainedLst_elem elem = root->start;
	while(elem != NULL) {
		ChainedLst_elem cur = elem;
		elem = elem->next;
		if( custom_free != NULL )
			custom_free(cur->data);
		free(cur);
	}
}

#endif /* end of include guard: UTILS_C_QRSWJMWI */
