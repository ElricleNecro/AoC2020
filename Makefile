CC=gcc

OUTDIR=.build
DAY=1
PART_DAY=1
INPUT=input

CFLAGS=-W -Wall -Wextra -g3 -I. -DINPUT_FILE=\"day$(DAY)/$(INPUT)\" -DDEBUG

.PHONY: run

$(OUTDIR)/day$(DAY)/c/part$(PART_DAY): day$(DAY)/c/part$(PART_DAY).c build_dir
	@$(CC) $(CFLAGS) $< -o $@

run: $(OUTDIR)/day$(DAY)/c/part$(PART_DAY)
	@./$^

build_dir:
	@mkdir -p $(OUTDIR)/day$(DAY)/c
