#!/usr/bin/env bash

set -e

usage() {
	echo -e "Usage: $1 [options] DAY_NUMBER"
	echo -e "Options:"
	echo -e "\t-h, --help: print this message."
	echo -e "\t-a, --all: add everything in the day directory."
	echo -e "\t-i, --info: add only brief, input and test data."
	echo -e "\t--c: add only c code."
	echo -e "\t--python: add only c code."
	echo -e "\t--custom ID NAME: add a custom language with id (day\${DAY_NUMBER}/\${ID}) and name (use in the commit message)."
	echo -e "\t--dry-run: run the script but don't execute anything."
}

# Commit brief, input and test data (if any).
# $1: day number to add.
add_info() {
	local day=$1

	$DBG git add day${day}/{brief,input}
	$DBG git commit -m "Added input and brief (with answer) for day${day}."

	$DBG git add day${day}/test*
	$DBG git commit -m "Adding test data for day${day}."
}

# Commit everything in the wanted day directory.
# $1: day number to add.
add_all() {
	local day=$1

	add_info ${day}

	$DBG git add day${day}/
	$DBG git commit -m "Adding every files for day${day}."
}

# Commit source files for a given language.
# $1: day number to add.
# $2: language id used to identify it (day$1/$2).
# $3: language name for the commit, $2 if not given.
add_language() {
	local day=$1
	local lang_id=$2
	local lang_name=${3:-${lang_id}}
	local dir=day${day}/${lang_id}

	if [[ ! -d "${dir}" ]]
	then
		echo "'${dir}' does not exist, doing nothing ${lang_name}." 1>&2
		return
	fi

	$DBG git add ${dir}
	$DBG git commit -m "Added ${lang_name} code for day${day}."
}

# Commit source files for c language.
# $1: day number to add.
add_c() {
	local day=$1

	add_language ${day} c
}

# Commit source files for python language.
# $1: day number to add.
add_py() {
	local day=$1

	add_language ${day} py python
}

PROG=$0
DAY=
DBG=
CALL=()
LANGUAGE_ID=()
LANGUAGE_NAME=()

if [[ "$#" -eq 0 ]]
then
	usage $PROG
	exit 1
fi

while [ "$#" -gt 0 ]
do
	case "$1" in
		-h|--help)
			usage "${PROG}"
			exit 0
			;;

		--dry-run)
			DBG=echo
			shift 1
			;;

		-a|--all)
			CALL+=(add_all)
			shift 1
			;;

		-i|--info)
			CALL+=(add_info)
			shift 1
			;;

		--c)
			CALL+=(add_c)
			shift 1
			;;

		--python)
			CALL+=(add_py)
			shift 1
			;;

		--custom)
			CALL+=(add_language)
			LANGUAGE_ID+=("$2")
			LANGUAGE_NAME+=("$3")
			shift 3
			;;

		-*|--*)
			echo "Unknown option $1." 1>&2
			usage "${PROG}"
			exit 1
			;;

		*)
			DAY=$1
			shift 1
			;;
	esac
done

if [[ "${DAY}" = "" ]]
then
	echo "Missing day number."
	usage "${PROG}"
	exit 1
fi

for callable in ${CALL[*]}
do
	if [[ "${callable}" = "add_language" ]]
	then
		${callable} "${DAY}" "${LANGUAGE_ID[0]}" "${LANGUAGE_NAME[0]}"
		unset LANGUAGE_ID[0]
		unset LANGUAGE_NAME[0]
	else
		${callable} "${DAY}"
	fi
done
