#include <stdio.h>
#include <stdlib.h>

/** Array struct. */
struct array {
	/** data */
	int *data;
	/** array size */
	size_t size;
};

/**
 * Create an array of given size.
 *
 * @param size Array size.
 * @return a struct array.
 */
struct array array_create(size_t size) {
	return (struct array){ .data = malloc(sizeof(int) * size), .size = size};
}

/**
 * Free the array.
 *
 * @param arr array to free.
 */
void array_free(struct array *arr) {
	free(arr->data);
	arr->size = 0;
}

/**
 * Read and parse a list of integer from a file.
 *
 * @param fname File to read
 * @return The array with the content.
 */
struct array read(const char* fname) {
	size_t size = 0;
	FILE *fich = fopen(fname, "r");

	while( fgetc(fich) != EOF )
		size++;

	rewind(fich);

	struct array array = array_create(size);
	for(size_t idx = 0; idx < array.size; idx++) {
		fscanf(fich, "%d", &array.data[idx]);
	}

	fclose(fich);

	return array;
}

int main(void) {
	struct array data = read("day1/input");

	for(size_t idx = 0; idx < data.size; idx++) {
		for(size_t jdx = 0; jdx < data.size; jdx++) {
			if( (data.data[idx] + data.data[jdx]) != 2020 )
				continue;

			printf("Answer: %d\n", data.data[idx] * data.data[jdx]);
			return EXIT_SUCCESS;
		}
	}

	return EXIT_FAILURE;
}
