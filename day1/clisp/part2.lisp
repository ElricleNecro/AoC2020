(load "utils.lisp")

(setq data (map 'list 'parse-integer (get-file "day1/input")))
(princ (apply '* (find-if (lambda (lst) (= 2020 (apply '+ lst))) (combinations data data data))))
