(load "utils.lisp")

(defun predicate (lst)
  (= 2020 (apply '+ lst)))

(setq data (map 'list 'parse-integer (get-file "day1/input")))
(princ (apply '* (find-if 'predicate (combinations data data))))
