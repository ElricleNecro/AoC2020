package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func ReadFile(fname string) []int {
	f, err := os.Open(fname)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	scanner.Split(bufio.ScanLines);

	var data []int;

	for scanner.Scan() {
		val, err := strconv.Atoi(scanner.Text());
		if err != nil {
			log.Fatal(err)
			return nil;
		}
		data = append(data, val);
	}
	err = scanner.Err()
	if err != nil {
		log.Fatal(err)
	}

	return data
}

func main() {
	data := ReadFile("day1/input");
	for _, x := range data {
		for _, y := range data {
			if (x + y) != 2020 {
				continue
			}

			fmt.Println(x * y)
			return
		}
	}
}
