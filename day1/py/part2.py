#!/usr/bin/env python
# coding: utf-8

from functools import reduce
from itertools import combinations

if __name__ == "__main__":
    with open("day1/input", "r") as fich:
        input = list(
            map(
                lambda x: int(x.strip()),
                fich.readlines(),
            )
        )

    for data in combinations(input, 3):
        if sum(data) != 2020:
            continue

        print("Answer:", reduce(lambda x, y: x * y, data))
        break
