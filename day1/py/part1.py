#!/usr/bin/env python
# coding: utf-8

from itertools import combinations

if __name__ == "__main__":
    with open("day1/input", "r") as fich:
        input = list(
            map(
                lambda x: int(x.strip()),
                fich.readlines(),
            )
        )

    for a, b in combinations(input, 2):
        if a + b != 2020:
            continue

        print(a, "*", b, "=", a * b)
        break
