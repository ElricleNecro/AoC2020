#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef INPUT_FILE
#define INPUT_FILE "day8/input"
#endif

#ifndef BUF_SIZE
#define BUF_SIZE 15
#endif

#include "utils.c"

static const char ACC_REPR[] = "acc";
static const char JMP_REPR[] = "jmp";
static const char NOP_REPR[] = "nop";

/**
 * Instruction set.
 */
typedef enum inst_e {
	ACC,
	JMP,
	NOP,

	ALL
} InstType;

/**
 * Instruction.
 */
typedef struct inst_t {
	InstType type;
	signed long arg;

	const char *repr;
} Inst;

typedef enum state_e {
	IDEN,
	ARG,
	NOPE
} StateType;

typedef struct state_t {
	char buffer[BUF_SIZE];
	size_t where;
	StateType type;
} State;

State state_init(void) {
	return (State){
		.buffer = {'\0'},
		.where = 0,
		.type = IDEN,
	};
}

State state_step(State state, const char c, Inst *inst) {
	switch(state.type) {
		case IDEN:
			if( c == ' ' ) {
				if( strcmp(ACC_REPR, state.buffer) == 0 ) {
					inst->repr = ACC_REPR;
					inst->type = ACC;
				} else if( strcmp(JMP_REPR, state.buffer) == 0 ) {
					inst->repr = JMP_REPR;
					inst->type = JMP;
				} else if( strcmp(NOP_REPR, state.buffer) == 0 ) {
					inst->repr = NOP_REPR;
					inst->type = NOP;
				}

				state.type = ARG;
				state.where = 0;
				memset(state.buffer, '\0', sizeof(state.buffer));
			} else {
				state.buffer[state.where] = c;
				state.where++;
			}
			break;

		case ARG:
			if( c == ' ' || c == '\n' ) {
				inst->arg = strtol(state.buffer, NULL, 10);

				state.type = NOPE;
				state.where = 0;
				memset(state.buffer, '\0', sizeof(state.buffer));
			} else {
				state.buffer[state.where] = c;
				state.where++;
			}
			break;

		case NOPE:
			break;
	}

	return state;
}

Inst inst_parse(const char *line) {
	Inst instruction;
	State state = state_init();

	for(size_t pos = 0; line[pos] != '\0'; pos++) {
		state = state_step(state, line[pos], &instruction);
	}

	return instruction;
}

/**
 * Handled CPU for day 8.
 */
typedef struct cpu_t {
	/** Instruction pointer: */
	size_t eip;
	size_t last_eip;

	/** Accumulator: */
	signed long acc;

	/** RAM: */
	Inst *code;
	size_t *executed;
	size_t mem_size;
} *Cpu;

Cpu cpu_new(void) {
	Cpu cpu = malloc(sizeof(struct cpu_t));

	cpu->eip = 0;
	cpu->last_eip = 0;
	cpu->acc = 0;
	cpu->code = NULL;
	cpu->mem_size = 0;

	return cpu;
}

void cpu_load(Cpu cpu, const char *input) {
	FILE *stream = fopen(input, "r");
	char buffer[BUF_SIZE] = {'\0'};
	size_t last_idx = 0;

	cpu->mem_size = line_count(stream);
	cpu->code = malloc(cpu->mem_size * sizeof(struct inst_t));
	cpu->executed = calloc(cpu->mem_size, sizeof(size_t));

	while( fgets(buffer, BUF_SIZE - 1, stream) != NULL ) {
		cpu->code[last_idx] = inst_parse(buffer);
		last_idx++;
	}

	fclose(stream);
}

void cpu_free(Cpu cpu) {
	if( cpu->code != NULL )
		free(cpu->code), cpu->code = NULL;

	free(cpu);
}

bool cpu_step(Cpu cpu) {
	if( cpu->eip >= cpu->mem_size )
		return false;

	cpu->executed[cpu->eip]++;
	if( cpu->executed[cpu->eip] >= 2 ) {
		cpu->executed[cpu->eip]--;

#ifdef DEBUG
		printf("Instruction 0x%03lX executed twice. Reverting to previous instruction (0x%03lX) and fixing it.", cpu->eip, cpu->last_eip);
#endif

		if( cpu->code[cpu->last_eip].type == JMP )
			cpu->code[cpu->last_eip].type = NOP;

		cpu->eip = cpu->last_eip;
	}

	cpu->last_eip = cpu->eip;
	Inst current = cpu->code[cpu->eip];
#ifdef DEBUG
	printf("0x%03lX: %s %ld (executed: %ld, ", cpu->eip, current.repr, current.arg, cpu->executed[cpu->eip]);
#endif
	switch(current.type) {
		case ACC:
			cpu->acc += current.arg;
			cpu->eip++;
			break;

		case JMP:
			cpu->eip += current.arg;
			break;

		case NOP:
			cpu->eip++;
			break;

		default:
			fprintf(
				stderr,
				"Unsupported instruction 0x%03lX:%s %ld (id %d).\n",
				cpu->eip,
				current.repr,
				current.arg,
				current.type
			);
			exit(EXIT_FAILURE);
	}
#ifdef DEBUG
	printf("%ld)\n", cpu->acc);
#endif

	return true;
}

int main(void) {
	Cpu handeld = cpu_new();
	cpu_load(handeld, INPUT_FILE);

#ifdef DEBUG
	printf("---------------------------- Loaded code:\n");
	for(size_t idx = 0; idx < handeld->mem_size; idx ++) {
		printf("0x%03lX:%s %ld\n", idx, handeld->code[idx].repr, handeld->code[idx].arg);
	}
	printf("-----------------------------------------\n");
#endif

	while( cpu_step(handeld) ) ;
	printf("Accumulator: %ld.\n", handeld->acc);

	cpu_free(handeld);

	return EXIT_SUCCESS;
}
