#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

enum Kind {
	GROUP,
	END_GROUP,
	EOL,
	END,

	ALL
};

struct state_t {
	/* SM data: */
	/** Found letters: */
	size_t yes[26];
	/** Number of unique answers: */
	size_t count;
	/** Number of person in the group: */
	size_t person;

	/** State: */
	enum Kind kind;

	/** Process the current state and change it if needed: */
	struct state_t (*process)(struct state_t);

	/** Get next token: */
	char (*get)(void*);
	/** Put back the token: */
	void (*put_back)(const char, void*);
	/** Data for the token stream: */
	void *data;
};

struct state_t group(struct state_t state);
struct state_t end_group(struct state_t state);
struct state_t eol(struct state_t state);

struct state_t new_state(char (*get)(void*), void (*pb)(const char, void*), void *data) {
	return (struct state_t) {
		.yes = {0},
		.count = 0,
		.person = 1,
		.kind = GROUP,
		.process = group,
		.get = get,
		.put_back = pb,
		.data = data,
	};
}

struct state_t group(struct state_t state) {
	const char token = state.get(state.data);

	if( token == '\n' ) {
		state.kind    = EOL;
		state.process = eol;

		return state;
	} else if( token == EOF ) {
		state.kind = END;
		state.process = NULL;
	}

	state.yes[token - 'a']++;

	return state;
}

struct state_t eol(struct state_t state) {
	const char token = state.get(state.data);

	if( token == '\n' ) {
		state.kind    = END_GROUP;
		state.process = end_group;

		return state;
	} else if( token == EOF ) {
		state.kind    = END;
		state.process = NULL;

		return state;
	}

	state.put_back(token, state.data);
	state.person          += 1;
	state.kind             = GROUP;
	state.process          = group;

	return state;
}

struct state_t end_group(struct state_t state) {
	for(size_t idx=0; idx < sizeof(state.yes) / sizeof(size_t); idx++) {
		if( state.yes[idx] == state.person ) {
			state.count++;
		}
		state.yes[idx] = 0;
	}

	state.person = 1;

	state.kind    = GROUP;
	state.process = group;

	return state;
}

char feeder(void *stream) {
	return fgetc((FILE*)stream);
}

void put_back(const char token, void *stream) {
	fseek((FILE*)stream, -1L, SEEK_CUR);
}

int main(void) {
	FILE *stream = fopen("day6/input", "r");
	struct state_t state = new_state(feeder, put_back, stream);

	while( state.kind != END )
		state = state.process(state);

	printf("Number of yes = %ld\n", state.count);

	fclose(stream);

	return EXIT_SUCCESS;
}
