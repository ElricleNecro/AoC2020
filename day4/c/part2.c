#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_VALID_FIELD 8
#define CONSIDERED_VALID 7

typedef enum kind {
	IDENT,
	SEP,
	VALUE,
	NEW_LINE,

	ALL
} Kind;

typedef struct state {
	Kind state;
	char ident[4];
	char value[1024];

	unsigned char valid_field;
	unsigned char ident_idx;
	unsigned char value_idx;
} State;

struct state state_create(void) {
	return (struct state){
		.state = NEW_LINE,
		.ident = {0},
		.value = {0},
		.valid_field = 0,
		.ident_idx = 0,
		.value_idx = 0,
	};
}

bool ident_is_valid(const char *ident) {
	if(
		strncmp("byr", ident, 3) == 0
		|| strncmp("iyr", ident, 3) == 0
		|| strncmp("eyr", ident, 3) == 0
		|| strncmp("hgt", ident, 3) == 0
		|| strncmp("hcl", ident, 3) == 0
		|| strncmp("ecl", ident, 3) == 0
		|| strncmp("pid", ident, 3) == 0
		/*|| strcmp("cid", ident) == 0*/
	)
		return true;

	return false;
}

/** `byr` (Birth Year) - four digits; at least `1920` and at most `2002`.*/
bool valide_byr(const char *value) {
	unsigned long year = strtol(value, NULL, 10);

	if( year < 1920 || year > 2002 )
		return false;

	return true;
}

/** `iyr` (Issue Year) - four digits; at least `2010` and at most `2020`.*/
bool valide_iyr(const char *value) {
	unsigned long year = strtol(value, NULL, 10);

	if( year < 2010 || year > 2020 )
		return false;

	return true;
}

/** `eyr` (Expiration Year) - four digits; at least `2020` and at most `2030`.*/
bool valide_eyr(const char *value) {
	unsigned long year = strtol(value, NULL, 10);

	if( year < 2020 || year > 2030 )
		return false;

	return true;
}

/** `hgt` (Height) - a number followed by either `cm` or `in`:*/
  /** If `cm`, the number must be at least `150` and at most `193`.*/
  /** If `in`, the number must be at least `59` and at most `76`.*/
bool valide_hgt(const char *value) {
	char *end = NULL;
	long hgt = strtol(value, &end, 10);

	if( strcmp("cm", end) == 0 && (150 <= hgt || hgt <= 193) )
		return true;
	else if( strcmp("in", end) == 0 && (59 <= hgt || hgt <= 76) )
		return true;

	return false;
}

/** `hcl` (Hair Color) - a `#` followed by exactly six characters `0`-`9` or `a`-`f`.*/
bool valide_hcl(const char *value) {
	if( strlen(value) != 7 )
		return false;

	if( value[0] != '#' )
		return false;

	for(size_t idx=1; idx < 7; idx++)
		if( (value[idx] < '0' || value[idx] > '9') && (value[idx] < 'a' || value[idx] > 'f') )
			return false;

	return true;
}

/** `ecl` (Eye Color) - exactly one of: `amb` `blu` `brn` `gry` `grn` `hzl` `oth`.*/
bool valide_ecl(const char *value) {
	if(
		strcmp("amb", value) == 0
		|| strcmp("blu", value) == 0
		|| strcmp("brn", value) == 0
		|| strcmp("gry", value) == 0
		|| strcmp("grn", value) == 0
		|| strcmp("hzl", value) == 0
		|| strcmp("oth", value) == 0
	)
		return true;

	return false;
}

/** `pid` (Passport ID) - a nine-digit number, including leading zeroes.*/
bool valide_pid(const char *value) {
	if( strlen(value) != 9 )
		return false;

	for(size_t idx=0; idx < 9; idx++)
		if( value[idx] <'0' || value[idx] > '9' )
			return false;

	return true;
}

bool valide(const char *ident, const char *value) {
	if( strncmp("byr", ident, 3) == 0 )
		return valide_byr(value);

	if( strncmp("iyr", ident, 3) == 0 )
		return valide_iyr(value);

	if( strncmp("eyr", ident, 3) == 0 )
		return valide_eyr(value);

	if( strncmp("hgt", ident, 3) == 0 )
		return valide_hgt(value);

	if( strncmp("hcl", ident, 3) == 0 )
		return valide_hcl(value);

	if( strncmp("ecl", ident, 3) == 0 )
		return valide_ecl(value);

	if( strncmp("pid", ident, 3) == 0 )
		return valide_pid(value);

	return false;
}

bool state_step(State *state, FILE *stream) {
	switch(state->state) {
		case IDENT: {
				const char c = fgetc(stream);
				if( c != ':' ) {
					state->ident[state->ident_idx++] = c;
				} else if( c == ':' ) {
					state->state = SEP;
				}

				break;
			}
		case SEP: {
				state->ident_idx = 0;
				state->state = VALUE;
				break;
			}
		case VALUE: {
				const char c = fgetc(stream);
				if( c == '\n' || c == ' ' ) {
					if( valide(state->ident, state->value) ) {
						state->valid_field++;
					}
					memset(state->value, 0, 1024);
					state->value_idx = 0;

					if( c == '\n' ) {
						state->state = NEW_LINE;
					} else if( c == ' ' ) {
						state->state = IDENT;
					}
				} else {
					state->value[state->value_idx++] = c;
				}

				break;
			}
		case NEW_LINE: {
				const char c = fgetc(stream);
				if( c == '\n' ) {
					return true;
				} else {
					fseek(stream, -1L, SEEK_CUR);
				}

				state->state = IDENT;
				break;
			}
		default: {
				fprintf(stderr, "Unknown state: '%d'.", state->state);
				exit(EXIT_FAILURE);
			}
	}

	return false;
}

int main(void) {
	FILE *stream = fopen("day4/input", "r");
	State state = state_create();
	size_t passport_valid = 0;
	size_t passport_count = 0;

	for(;;) {
		if( state_step(&state, stream) ) {
			passport_count++;
			if( state.valid_field >= CONSIDERED_VALID )
				passport_valid++;
			state.valid_field = 0;
		}
		const char c = fgetc(stream);
		if( c == EOF )
			break;
		fseek(stream, -1L, SEEK_CUR);
	}

	// TODO: I am one too high...
	printf("Found %ld/%ld valid password.\n", passport_valid - 1, passport_count);

	fclose(stream);

	return EXIT_SUCCESS;
}
