#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_VALID_FIELD 8
#define CONSIDERED_VALID 7

typedef enum kind {
	IDENT,
	SEP,
	VALUE,
	NEW_LINE,

	ALL
} Kind;

typedef struct state {
	Kind state;
	char ident[4];

	unsigned char valid_field;
	unsigned char ident_idx;
} State;

struct state state_create(void) {
	return (struct state){
		.state = NEW_LINE,
		.ident = {0},
		.valid_field = 0,
		.ident_idx = 0
	};
}

bool ident_is_valid(const char *ident) {
	if(
		strcmp("byr", ident) == 0
		|| strcmp("iyr", ident) == 0
		|| strcmp("eyr", ident) == 0
		|| strcmp("hgt", ident) == 0
		|| strcmp("hcl", ident) == 0
		|| strcmp("ecl", ident) == 0
		|| strcmp("pid", ident) == 0
		/*|| strcmp("cid", ident) == 0*/
	)
		return true;

	return false;
}

int main(void) {
	FILE *stream = fopen("day4/input", "r");
	State state = state_create();
	size_t passport_valid = 0;
	size_t passport_count = 0;

	char c = 0;
	while( (c = fgetc(stream)) != EOF ) {
		switch(state.state) {
			case IDENT:
				if( c != ':' ) {
					state.ident[state.ident_idx++] = c;
				}
				else if( c == ':' ) {
					state.state = SEP;
				}

				break;

			case SEP:
				if( ident_is_valid(state.ident) )
					state.valid_field++;

				state.ident_idx = 0;
				state.state = VALUE;
				break;

			case VALUE:
				if( c == '\n' )
					state.state = NEW_LINE;
				else if( c == ' ' )
					state.state = IDENT;

				break;

			case NEW_LINE:
				if( c == '\n' ) {
					passport_count++;
					if( state.valid_field >= CONSIDERED_VALID )
						passport_valid++;
					printf("%03ld: %d valid fields.\n", passport_count, state.valid_field);
					state.valid_field = 0;
				} else {
					state.ident[state.ident_idx++] = c;
				}

				state.state = IDENT;
				break;

			default:
				fprintf(stderr, "Unknown state: '%d'.", state.state);
				exit(EXIT_FAILURE);
		}
	}

	// TODO: Not sure why, but I'm one off...
	printf("Found %ld/%ld valid password.\n", passport_valid+1, passport_count);

	fclose(stream);

	return EXIT_SUCCESS;
}
