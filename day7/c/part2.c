#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef BUF_SIZE
#define BUF_SIZE 2048
#endif

#ifndef INPUT_FILE
#define INPUT_FILE "day7/input"
#endif

#ifndef MAIN_BAG
#define MAIN_BAG "shiny gold"
#endif

#include "utils.c"

typedef struct rule_t {
	/** Color name: */
	char *color;

	/** Number of child: */
	size_t child_number;
	/** Children color name: */
	char **children;
	/** Number of bag per children: */
	size_t *many;
} Rule;

void rule_free(void *data) {
	Rule *rule = (Rule*)data;
	for(size_t idx=0; idx < rule->child_number; idx++)
		free(rule->children[idx]);

	free(rule->children);
	free(rule->color);
	free(rule->many);
}

enum StateKind {
	MAIN_COLOR,
	SUB_COLOR,
	NUMBER,
	BAG,
	CHILD,
	CONTAIN,
	EMPTY,
	NONE,

	ALL
};

enum StateKind process(enum StateKind kind, const char *word) {
	if( strcmp("bag,", word) == 0 || strcmp("bags,", word) == 0 )
		return CONTAIN;

	if( strcmp("bag.", word) == 0 || strcmp("bags.", word) == 0 )
		return CHILD;

	if( strcmp("bags", word) == 0 )
		return BAG;

	if( strcmp("contain", word) == 0 )
		return CONTAIN;

	if( strcmp("no", word) == 0 )
		return EMPTY;

	char *endptr = NULL;
	strtoul(word, &endptr, 10);
	if( *endptr == '\0' ) //value != LONG_MIN && value != LONG_MAX )
		return NUMBER;

	return kind;
}

Rule* parse_rule(const char *buffer) {
	Rule *rule = malloc(sizeof(Rule));
	rule->color = calloc(60, sizeof(char));
	rule->child_number = 0;
	rule->children = NULL;
	rule->many = NULL;

	char word[60] = {'\0'};
	enum StateKind kind = MAIN_COLOR;

	size_t offset = 0;
	while( offset < strlen(buffer) ) {
		sscanf(&buffer[offset], "%s", word);
		offset += strlen(word) + 1;

		kind = process(kind, word);

		switch(kind) {
			case MAIN_COLOR:
				if( rule->color[0] != '\0' )
					strcat(rule->color, " ");
				strcat(rule->color, word);
				break;

			case SUB_COLOR:
				if( rule->children[rule->child_number - 1][0] != '\0' )
					strcat(rule->children[rule->child_number - 1], " ");
				strcat(rule->children[rule->child_number - 1], word);
				break;

			case NUMBER:
				rule->many[rule->child_number - 1] = strtoul(word, NULL, 10);
				kind = SUB_COLOR;
				break;

			case BAG:
				rule->color = realloc(
					rule->color,
					(strlen(rule->color) + 1) * sizeof(char)
				);
				break;

			case CHILD:
				if( rule->child_number == 0 ) break;
				rule->children[rule->child_number - 1] = realloc(
					rule->children[rule->child_number - 1],
					(strlen(rule->children[rule->child_number - 1]) + 1) * sizeof(char)
				);
				break;

			case CONTAIN:
				rule->child_number++;
				rule->children = realloc(
					rule->children,
					rule->child_number * sizeof(char*)
				);
				rule->many = realloc(
					rule->many,
					rule->child_number * sizeof(int*)
				);
				rule->children[rule->child_number - 1] = calloc(60, sizeof(char));
				break;

			case EMPTY:
				for(size_t idx=0; idx < rule->child_number; idx++)
					free(rule->children[idx]);
				rule->child_number = 0;
				free(rule->children), rule->children = NULL;
				free(rule->many), rule->many = NULL;
				break;

			case NONE:
				offset = strlen(buffer);
				break;

			default:
				fprintf(stderr, "Unrecognised state '%d'.\n", kind);
				exit(EXIT_FAILURE);
		}
	}

	return rule;
}

#define FOREACH_RULES(var, from, code) LST_FOREACH(Rule*, var, from, code)
#define FOREACH_COLOR(var, from, code) LST_FOREACH(const char*, var, from, code)
#define PRINT_COLORS(from) {\
	printf("[ "); \
	for(ChainedLst_elem cur = from->start; cur != NULL; cur = cur->next) { \
		printf("%s", (const char*)cur->data); \
		if( cur->next != NULL ) \
			printf(", "); \
	} \
	printf(" ]\n"); \
}

#define FOREACH_RULE_CHILD(var, count, from, code) { \
	for(size_t idx = 0; idx < from->child_number; idx ++) { \
		char *var = from->children[idx]; \
		size_t count = from->many[idx]; \
		code; \
	} \
}

Rule* search(ChainedLst rules, const char *key) {
	FOREACH_RULES(rule, rules, { if( strcmp(rule->color, key) == 0 ) return rule;});

	return NULL;
}

bool cmp_str(void *a, void *b) {
	return strcmp(a, b) == 0;
}

size_t count_container(ChainedLst rules, const char *color) {
	size_t sum = 1;
	Rule* current = search(rules, color);

	FOREACH_RULE_CHILD(color, many, current, {
		sum += many * count_container(rules, color);
	});

	return sum;
}

ChainedLst parse_input(const char *input) {
	FILE *stream = fopen(input, "r");

	char buffer[BUF_SIZE] = {'\0'};
	ChainedLst rules = NULL;

	while( fgets(buffer, BUF_SIZE - 1, stream) != NULL ) {
		rules = lst_append(
			rules,
			parse_rule(buffer)
		);
	}

	fclose(stream);

	return rules;
}

int main(void) {
	ChainedLst rules = parse_input(INPUT_FILE);

	printf(
		"Our '%s' bag is supposed to contain %lu bags.\n",
		MAIN_BAG,
		count_container(rules, MAIN_BAG) - 1
	);

	lst_free(rules, rule_free);

	return EXIT_SUCCESS;
}
