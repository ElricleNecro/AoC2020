#!/usr/bin/env python
# coding: utf-8

from collections import namedtuple
from re import compile
from typing import List, Pattern


Data = namedtuple('Data', ['min', 'max', 'c', 'passwd'])
_REGEXP: Pattern = compile("(?P<min>[0-9]+)-(?P<max>[0-9]+) (?P<c>.): (?P<passwd>.*)")


def parse(input: str) -> List[Data]:
    """Parse the list of password constraint and password to check.

    :param input: file to read.
    :type input: str
    :return: The list of password and policy.
    :rettype: list of Data.
    """
    with open(input, "r") as stream:
        return list(
            map(
                lambda line: Data(int(line[0]), int(line[1]), line[2], line[3]),
                _REGEXP.findall(stream.read())
            )
        )


def policy(data: Data) -> bool:
    """Does the given Data fit the company policy for password?
    :param data: Data to check.
    :type data: Data
    :return: True if it respect the policy, else False
    :rettype: bool
    """
    count = len(
        list(
            filter(
                lambda s: s == data.c,
                data.passwd
            )
        )
    )

    return data.min <= count <= data.max


if __name__ == "__main__":
    data = parse("day2/input")
    valid = len(
        list(
            filter(
                lambda b: b,
                map(
                    lambda d: policy(d),
                    data
                )
            )
        )
    )
    print("There is", valid, "valid passwords.")
