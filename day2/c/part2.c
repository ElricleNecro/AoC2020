#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUF_SIZE 1024

typedef struct _data {
	size_t min, max;
	char c;
	char *passwd;
} Data;

size_t line_count(FILE *stream) {
	size_t lines = 0;
	char c = EOF, prev = EOF;

	for(c=fgetc(stream), prev=EOF; c != EOF; prev = c, c = fgetc(stream) )
		if( c == '\n' )
			lines++;

	if( prev != '\n' )
		lines++;

	rewind(stream);

	return lines;
}

Data* parse(const char *fname, size_t *lines) {
	FILE *stream = fopen(fname, "r");
	*lines = line_count(stream);
	Data *data = malloc(*lines * sizeof(struct _data));

	for(size_t idx = 0; idx < *lines; idx++) {
		data[idx].passwd = calloc(BUF_SIZE, sizeof(char));
		fscanf(stream, "%ld-%ld %c: %1024s\n", &data[idx].min, &data[idx].max, &data[idx].c, data[idx].passwd);
		data[idx].passwd = realloc(data[idx].passwd, strlen(data[idx].passwd));
	}

	return data;
}

int main(void) {
	size_t size = 0;
	Data *data = parse("day2/input", &size);

	size_t valid = 0;
	for(size_t idx = 0; idx < size; idx++) {
		const char *passwd = data[idx].passwd;
		if( ((passwd[data[idx].min - 1] == data[idx].c) && (passwd[data[idx].max - 1] != data[idx].c))
			|| ((passwd[data[idx].min - 1] != data[idx].c) && (passwd[data[idx].max - 1] == data[idx].c)) )
			valid++;
	}
	printf("There is %ld valid password.\n", valid);

	for(size_t idx = 0; idx < size; idx++)
		free(data[idx].passwd);
	free(data);

	return EXIT_SUCCESS;
}
