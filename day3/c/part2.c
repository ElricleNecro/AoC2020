#include <stdio.h>
#include <stdlib.h>

#include "utils.c"

struct slopes {
	size_t right, down;
};

static const struct slopes SLOPES[] = {
    {.right = 1, .down = 1},
    {.right = 3, .down = 1},
    {.right = 5, .down = 1},
    {.right = 7, .down = 1},
    {.right = 1, .down = 2},
};

size_t line_length(FILE *stream) {
	size_t len = 0;
	char c = 0;

	while( (c = fgetc(stream)) != '\n' )
		len++;

	rewind(stream);

	return len;
}

struct array parse(const char *fname, size_t *width) {
	FILE *stream = fopen(fname, "r");
	size_t height = line_count(stream);
	struct array map = array_create(height * sizeof(char*));

	*width = line_length(stream);
	map.size = height;

	for(size_t idx=0; idx < height; idx++) {
		// Allocating the row:
		*((char**)map.data + idx) = malloc(*width + 1 * sizeof(char));
		// Filling it:
		fgets(*((char**)map.data + idx), *width + 1, stream);
		// Reading the last character (should be '\n' or EOF):
		char c = fgetc(stream);
		if( c != '\n' && c != EOF )
			fprintf(stderr, "Error, read '%c' instead of '\\n' or 'EOF'.\n", c);
	}

	return map;
}

int main(void) {
	size_t width = 0;
	size_t total = 1;
	struct array map = parse("day3/input", &width);

	// right -> 3, down -> 1;
	// '.': empty, '#': tree
	for(size_t slope_idx=0; slope_idx < (sizeof(SLOPES) / sizeof(struct slopes)); slope_idx++) {
		size_t right = SLOPES[slope_idx].right;
		size_t down  = SLOPES[slope_idx].down;
		size_t column = 0;
		size_t nb_tree = 0;

		for(size_t idx=0; idx < map.size; idx += down) {
			if( *(*((char**)map.data + idx) + column) == '#' )
				nb_tree++;
			column = (column + right) % width;
		}

		total *= nb_tree;
	}

	printf("Found %ld tree.\n", total);

	for(size_t idx = 0; idx < map.size; idx++) {
		free(*((char**)map.data + idx));
	}
	array_free(&map);

	return EXIT_SUCCESS;
}
