#!/usr/bin/env python
# coding: utf-8

from typing import List


def parse(input: str) -> List[str]:
    with open(input, "r") as fich:
        return list(
            map(
                lambda line: line.strip(),
                fich.readlines()
            )
        )


if __name__ == "__main__":
    data = parse("day3/input")
    col = 0
    nb_tree = 0

    for row in data:
        if row[col] == '#':
            nb_tree += 1
        col += 3
        col %= len(row)

    print("Found", nb_tree, "trees.")
