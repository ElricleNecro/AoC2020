#!/usr/bin/env python
# coding: utf-8

from typing import List, Tuple


SLOPES: List[Tuple[int, int]] = [
    (1, 1),
    (3, 1),
    (5, 1),
    (7, 1),
    (1, 2),
]


def parse(input: str) -> List[str]:
    with open(input, "r") as fich:
        return list(
            map(
                lambda line: line.strip(),
                fich.readlines()
            )
        )


if __name__ == "__main__":
    data = parse("day3/input")
    total = 1

    for right, down in SLOPES:
        col = 0
        nb_tree = 0

        for row in data[::down]:
            if row[col] == '#':
                nb_tree += 1
            col += right
            col %= len(row)

        total *= nb_tree

    print("Found", total, "trees.")
