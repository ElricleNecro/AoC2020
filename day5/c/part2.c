#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define ROW_MAX 127
#define COL_MAX 7

int main(int argc, char *argv[]) {
	FILE *stream = fopen("day5/input", "r");
	char c = EOF;
	size_t row_min = 0, row_max = ROW_MAX;
	size_t col_min = 0, col_max = COL_MAX;
	bool present[(ROW_MAX+1) * (COL_MAX+1)] = {false};

	while( (c = fgetc(stream)) != EOF ) {
		switch(c) {
			case 'F':
				row_max = row_min + (row_max - row_min) / 2;
				/*printf("F: [%ld; %ld], ", row_min, row_max);*/
				break;
			case 'B':
				row_min = row_max - (row_max - row_min) / 2;
				/*printf("B: [%ld; %ld], ", row_min, row_max);*/
				break;
			case 'L':
				col_max = col_min + (col_max - col_min) / 2;
				/*printf("F: [%ld; %ld], ", col_min, col_max);*/
				break;
			case 'R':
				col_min = col_max - (col_max - col_min) / 2;
				/*printf("B: [%ld; %ld], ", col_min, col_max);*/
				break;
			case '\n':
				{
					/*printf("\n[%ld; %ld], [%ld; %ld]\n", row_min, row_max, col_min, col_max);*/
					present[row_max * 8 + col_max] = true;
					row_min = 0;
					row_max = ROW_MAX;
					col_min = 0;
					col_max = COL_MAX;
				}
				break;
			default:
				fprintf(stderr, "Error while parsing file: unrecognised character '%c'.\n", c);
				exit(EXIT_FAILURE);
		}
	}

	size_t seat = 0;
	for(seat = 1; seat < (sizeof(present) / sizeof(bool) - 1); seat++)
		if( !present[seat] && present[seat-1] && present[seat+1] )
			break;

	printf("My seat is: %ld.\n", seat);

	fclose(stream);

	return EXIT_SUCCESS;
}
