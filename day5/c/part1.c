#include <stdio.h>
#include <stdlib.h>

#define ROW_MAX 127
#define COL_MAX 7

int main(int argc, char *argv[]) {
	FILE *stream = fopen("day5/input", "r");
	char c = EOF;
	size_t max = 0;
	size_t row_min = 0, row_max = ROW_MAX;
	size_t col_min = 0, col_max = COL_MAX;

	while( (c = fgetc(stream)) != EOF ) {
		switch(c) {
			case 'F':
				row_max = row_min + (row_max - row_min) / 2;
				break;
			case 'B':
				row_min = row_max - (row_max - row_min) / 2;
				break;
			case 'L':
				col_max = col_min + (col_max - col_min) / 2;
				break;
			case 'R':
				col_min = col_max - (col_max - col_min) / 2;
				break;
			case '\n':
				{
					size_t seat = row_max * 8 + col_max;
					if( seat > max )
						max = seat;
					row_min = 0;
					row_max = ROW_MAX;
					col_min = 0;
					col_max = COL_MAX;
				}
				break;
			default:
				fprintf(stderr, "Error while parsing file: unrecognised character '%c'.\n", c);
				exit(EXIT_FAILURE);
		}
	}

	printf("Higher seat number found: %ld.\n", max);

	fclose(stream);

	return EXIT_SUCCESS;
}
